// ignore_for_file: prefer_const_constructors

import 'dart:html';

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text(
            "Vaksinasi",
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
          backgroundColor: Color.fromRGBO(202, 195, 190, 1),
        ),
        body: Container(
          margin: EdgeInsets.all(15),
          child: Column(
            children: const <Widget>[
              Text(
                "Apa itu vaksin?",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              Text(
                "Vaksin adalah sediaan biologis yang digunakan untuk menghasilkan kekebalan adaptif terhadap penyakit infeksi tertentu.",
              ),
              Text(
                "Biasanya, vaksin mengandung agen atau zat yang menyerupai mikroorganisme penyebab penyakit dan sering kali dibuat dari",
              ),
              Text(
                "merangsang sistem imun agar dapat mengenali agen tersebut sebagai ancaman, menghancurkannya, dan mengingatnya agar sistem imun",
              ),
              Text(
                "dapat kembali mengenali dan menghancurkan mikroorganisme yang berhubungan dengan agen tersebut saat ditemui pada masa depan.",
              ),
              Text(
                "Mengapa perlu vaksin?",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              Text(
                "Mengingat akan penyakit Covid-19 yang sedang mewabah saat ini dan telah memakan banyak korban jiwa, vaksinasi mengambil",
              ),
              Text(
                "peran penting dalam pembentukan herd immunity di masyarakat. Herd immunity merupakan suatu kondisi ketika",
              ),
              Text(
                "sebagian besar populasi kebal terhadap penyakit menular tertentu sehingga memberikan perlindungan tidak langsung kepada",
              ),
              Text(
                "kelompok masyarakat yang tidak kebal terhadap penyakit menular tersebut. Hal ini dapat mengurangi tingkat penularan yang",
              ),
              Text(
                "kemudian juga mengurangi angka kematian yang terjadi.",
              ),
              Text(
                "Progress vaksinasi per kategori",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              Text(
                "Pilih kategori: ",
              ),
            ],
            crossAxisAlignment: CrossAxisAlignment.start,
          ),
        ),
        backgroundColor: Color.fromRGBO(237, 234, 229, 1),
      ),
    );
  }
}
