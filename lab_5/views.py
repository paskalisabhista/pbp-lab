from django.shortcuts import render, get_object_or_404, HttpResponseRedirect
from lab_2.models import Note
from .forms import NoteForm

def index(request):
    notes = Note.objects.all().values()
    context = {'notes': notes}
    return render(request, 'lab5_index.html', context)

def get_note(request, id):
    context = {}
    context["note"] = Note.objects.get(id = id)
    return render(request, 'lab5_view.html', context)

def update_note(request, id):
    context = {}
    obj = get_object_or_404(Note, id = id)
    form = NoteForm(request.POST or None, instance = obj)

    if form.is_valid():
        form.save()
        return HttpResponseRedirect("/lab-5")

    context["form"] = form
    return render(request, "lab5_update.html", context)

def delete_note(request, id):
    context = {}
    obj = get_object_or_404(Note, id = id)

    if request.method == "POST":
        obj.delete()
        return HttpResponseRedirect("/lab-5")

    return render(request, "lab5_delete.html", context)