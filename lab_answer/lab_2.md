1. Apakah perbedaan antara JSON dan XML?

# JSON
- Hanyalah format yang di tulis dalam JavaScript
- Implementasi penulisannya menggunakan dictionary / map
- Mendukung baik UTF maupun ASCII
- Dapat mengimplementasikan array
- Data disimpan dalam pasangan key dan value

# XML
- Merupakan bahasa markup, bukan bahasa pemrograman
- Implementasi menggunakan tag layaknya HTML
- Mendukung pengkodean UTF-8 dan UTF-16
- Apabila ingin menggunakan array implementasinya menggunakan tag sehingga sulit untuk dibaca
- Data disimpan layaknya struktur pohon (root, branch, leaves)

2. Apakah perbedaan antara HTML dan XML?

# HTML
- Merupakan bahasa markup yang berfokus pada tampilan web baik teks, gambar, maupun video
- Tag pada HTML berjumlah terbatas
- Tidak terlalu strict terhadap tag penutup (ada beberapa tag yang tidak menggunakan penutup)
- Case insensitive (perbedaan huruf besar dan kecil tidak merubah makna)

# XML
- Merupakan bahasa markup yang berfokus pada pengangkutan data tetapi tidak menampilkannya
- Tag pada XML dapat bervariasi tidak terbatas
- Strict terhadap tag penutup
- Case sensitive (perbedaan huruf besar dan kecil dapat merubah makna)