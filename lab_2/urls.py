from django.urls import path
from .views import index, xml, json

app_name = "applab2"
urlpatterns = [
    path('', index, name='index'),
    path('xml', xml, name='xml'),
    path('json', json, name='json'),
]
