from django.db import models


class Note(models.Model):
    to_who = models.CharField(verbose_name = "To", max_length=30)
    from_who = models.CharField(verbose_name = "From", max_length=30)
    title = models.CharField(verbose_name = "Title", max_length=30)
    message = models.CharField(verbose_name = "Message", max_length=200)