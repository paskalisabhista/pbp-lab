// ignore_for_file: prefer_const_constructors

import 'dart:html';

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text(
            "Vaksinasi",
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
          backgroundColor: Color.fromRGBO(202, 195, 190, 1),
        ),
        body: Container(
          margin: EdgeInsets.all(15),
          child: Column(
            children: const <Widget>[
              Text(
                "Apa itu vaksin?",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              Text(
                "Vaksin adalah sediaan biologis yang digunakan untuk menghasilkan kekebalan adaptif terhadap penyakit infeksi tertentu.",
              ),
              Text(
                "Biasanya, vaksin mengandung agen atau zat yang menyerupai mikroorganisme penyebab penyakit dan sering kali dibuat dari",
              ),
              Text(
                "merangsang sistem imun agar dapat mengenali agen tersebut sebagai ancaman, menghancurkannya, dan mengingatnya agar sistem imun",
              ),
              Text(
                "dapat kembali mengenali dan menghancurkan mikroorganisme yang berhubungan dengan agen tersebut saat ditemui pada masa depan.",
              ),
              Text(
                "Mengapa perlu vaksin?",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              Text(
                "Mengingat akan penyakit Covid-19 yang sedang mewabah saat ini dan telah memakan banyak korban jiwa, vaksinasi mengambil",
              ),
              Text(
                "peran penting dalam pembentukan herd immunity di masyarakat. Herd immunity merupakan suatu kondisi ketika",
              ),
              Text(
                "sebagian besar populasi kebal terhadap penyakit menular tertentu sehingga memberikan perlindungan tidak langsung kepada",
              ),
              Text(
                "kelompok masyarakat yang tidak kebal terhadap penyakit menular tersebut. Hal ini dapat mengurangi tingkat penularan yang",
              ),
              Text(
                "kemudian juga mengurangi angka kematian yang terjadi.",
              ),
              Text(
                "Progress vaksinasi per kategori",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              Text(
                "Pilih kategori: ",
              ),
              MyStatefulWidget(),
            ],
            crossAxisAlignment: CrossAxisAlignment.start,
          ),
        ),
        backgroundColor: Color.fromRGBO(237, 234, 229, 1),
      ),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  String dropdownValue = 'Semua Vaksinasi';

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        DropdownButton<String>(
          dropdownColor: Colors.white,
          value: dropdownValue,
          icon: const Icon(Icons.expand_more),
          iconSize: 24,
          elevation: 16,
          style: const TextStyle(color: Colors.black),
          underline: Container(
            height: 2,
          ),
          onChanged: (String? newValue) {
            setState(() {
              dropdownValue = newValue!;
              print(newValue);
            });
          },
          items: <String>[
            'Semua Vaksinasi',
            'Tenaga Kesehatan',
            'Petugas Publik',
            'Lanjut Usia',
            'Masyarakat Rentan dan Umum',
            'Kelompok Usia 12-17 Tahun',
          ].map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(value),
            );
          }).toList(),
        ),
        Text("Value yang dipilih: " + dropdownValue),
      ],
    );
  }
}
